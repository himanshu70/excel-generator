package com.crater.excelgenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExcelGeneratorServiceImpl implements ExcelGeneratorService{
	
	private static final String excelDirectory = "/tmp/";
	
	private final String titleFont;
	private final String titleFontSize;
	private final String titleBold;
	private final String bodyFontSize;
	private final String bodyFont;
	private final String bodyBold;
	
	
	@Autowired
	public ExcelGeneratorServiceImpl(@Value("${title.font}") String titleFont,   
			@Value("${title.font.size}") String titleFontSize ,@Value("${title.bold}") String titleBold,
			@Value("${body.font}") String bodyFont, 
			@Value("${body.font.size}") String bodyFontSize ,@Value("${body.bold}") String bodyBold){
		this.titleFont=titleFont;
		this.titleBold=titleBold;
		this.titleFontSize=titleFontSize;
		this.bodyFont=bodyFont;
		this.bodyBold=bodyBold;
		this.bodyFontSize=bodyFontSize;
	}
			 
		

	public File createExcel(final List<HashMap<Integer, String>>  dataList,final String fileName) throws IOException{
		final Integer columnCount=dataList.get(0).size();
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Excel sheet");
        int num=0;
		for(HashMap<Integer, String> word:dataList){
			HSSFRow row = sheet.createRow(num++);
			for(int index=0;index<word.size();index++){
				if(word.get(index)!=null){
				if(num==1){
					row.createCell(index).setCellValue(word.get(index).toString());
		        	row.getCell(index).setCellStyle(getTitleStyle(workbook,titleFont,titleFontSize,titleBold));
				   }else{
					   row.createCell(index).setCellValue(word.get(index).toString());
					   row.getCell(index).setCellStyle(getBodyStyle(workbook,bodyFont,bodyFontSize,bodyBold));
					}
				}
	     	}
		}
	   autosizeColumn(sheet,columnCount);
	   clearExcelDirectory(excelDirectory,".xlsx");
	   final File f=new File(excelDirectory+fileName+".xlsx");
	   final FileOutputStream fileOut = new FileOutputStream(f);
	   workbook.write(fileOut);
	   fileOut.flush();
	   fileOut.close();
	   return f;
     }
	
	/**
	 *This will set column size to the maximum of the size of data in the column.
	 */
	private void autosizeColumn(final HSSFSheet sheet, final Integer columnCount) {
		   for(int index=0 ;index<columnCount;index++){
			sheet.autoSizeColumn(index);
		   }
    }


	/*
	 * For setting data properties present in  the header section of excel sheet
	 */
	private HSSFCellStyle getTitleStyle(final HSSFWorkbook workbook,final  String titleFont, final String titleFontSize, final String titleBold) {
		HSSFCellStyle titleStyle = workbook.createCellStyle();
		titleStyle.setWrapText(true);
        HSSFFont font = workbook.createFont();
        font.setFontName(titleFont);
        font.setFontHeightInPoints((short)Integer.parseInt(titleFontSize));
        font.setBoldweight(Short.valueOf(titleBold));
        titleStyle.setFont(font);
        return titleStyle;
		
	}


	/*
	 * For setting data properties present in  the body section of excel sheet
	 */
	private HSSFCellStyle getBodyStyle(final HSSFWorkbook workbook,final String bodyFont,final  String bodyFontSize,final String bodyBold) {
		    HSSFCellStyle bodyStyle = workbook.createCellStyle();
	        bodyStyle.setWrapText(true);
	        HSSFFont bFont = workbook.createFont();
	        bFont.setFontName(bodyFont);
	        bFont.setFontHeightInPoints((short)Integer.parseInt(bodyFontSize));
	        bFont.setBoldweight(Short.valueOf(bodyBold));
	        bodyStyle.setFont(bFont);
	        return bodyStyle;
	}

	/*
	 * For clearing excel directory
	 */
	private static void clearExcelDirectory(final String excelDirectory, final String extension) {
		final File dir = new File(excelDirectory);
		Arrays.stream(dir.listFiles((f, p) -> p.endsWith(extension))).forEach(File::delete);    		
	}

	
}
