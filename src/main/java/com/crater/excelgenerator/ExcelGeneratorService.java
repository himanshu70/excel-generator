package com.crater.excelgenerator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface ExcelGeneratorService {

	
	File createExcel(final List<HashMap<Integer, String>>  dataList,final String fileName) throws IOException;
}
